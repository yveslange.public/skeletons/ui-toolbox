import { LitElement, html, css } from 'lit'
import { ElementMixins, importScript, importCSS } from './mixins.js'

importScript('https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js')
importScript('https://www.gstatic.com/firebasejs/8.8.1/firebase-auth.js')
importScript('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js')
importCSS('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css')

class ElementFirebaseAuth extends ElementMixins(LitElement) {
  static get styles () {
    return css`
      :host {
        display: flex;
        position: relative;
        font-family: Roboto, Arial;
      }
      .width {
        width: 250px;
      }
      [hidden] {
        display: none !important;
      }
      #wrapper {
        display: flex;
        flex-grow: 0;
        flex-direction: column;
      }
      #profile {
        display: flex;
        flex-direction: row;
        align-items: center;
        cursor: pointer;
      }
      #profile #profilePicture {
        width: 20px;
        height: 20px;
        border-radius: 50px;
        border: 3px solid #eee;
        margin-right: 5px;
      }
      #profile #profileName {
        margin-right: 5px;
      }
      #profile #profileName:hover {
        text-decoration: underline;
      }
      .title {
        font-size: 28px;
        font-weight: bold;
        margin: 15px 0;
      }
      #loginBox {
        color: #333;
        box-shadow: 1px 1px 10px 1px #aaa;
        position: absolute;
        top: 28px;
        border: 1px solid #eee;
        border-radius: 10px;
        display: flex;
        flex-grow: 1;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background-color: white;
      }
      #loginBox #close {
        cursor: pointer;
        position: absolute;
        top: 5px;
        right: 8px;
      }
      #loginBox #logoutBtn {
        padding: 10px;
        cursor: pointer;
      }
      #loginBox #logoutBtn:hover {
        color: #333;
        text-decoration: underline;
      }
      #loginOptions {
        text-align: center;
      }
      #loginOptions .loginBtn {
        display: flex;
        margin: 5px;
        padding: 10px 20px;
        text-align: center;
        border: 1px solid #eee;
        cursor: pointer;
      }
      #loginOptions .loginBtn:hover {
        border: 1px solid #aaa;
      }
      #loginOptions .loginBtn i {
        margin-right: 10px;
      }
      #loginUserForm,
      #createUserForm {
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      #loginUserForm input,
      #createUserForm input {
        margin-top: 5px;
      }
      #loginUserForm button[name='submit'],
      #createUserForm button[name='submit'] {
        margin-top: 10px;
        margin-bottom: 15px;
      }
      #loginUserForm #loginUserError,
      #createUserForm #createUserError {
        margin: 5px;
        color: red;
        font-weight: bold;
        font-size: 12px;
      }

      #loginUserForm #passwordForgot {
        font-size: 9px;
        margin-bottom: 10px;
      }
      #loginUserForm #passwordForgot:hover {
        text-decoration: underline;
        cursor: pointer;
      }
      #forgotPasswordMsg {
        padding: 15px;
      }
      #profileEmail {
        padding: 15px;
      }
    `
  }

  render () {
    return html`
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" />
      <div id="wrapper">
        <div id="profile">
          <div id="profileName" @click=${this._displayLoginBox}>${this.isLogged ? this.profileName : 'Anonymous'}</div>
          <img
            id="profilePicture"
            src=${this.profilePicture ? this.profilePicture : 'https://www.gravatar.com/avatar/?d=mp'}
            @click=${this._displayLoginBox}
          />
        </div>
        <div id="loginBox" class="width" hidden>
          <i id="close" class="fas fa-close" @click=${this._hideLoginBox}></i>
          <div id="loginOptions" ?hidden=${this._loginBoxStep !== 'loginOptions' || this.isLogged}>
            <div class="title">Login</div>
            <div id="loginGoogleBtn" class="loginBtn" @click=${this.loginGoogle}>
              <i class="fab fa-google"></i> Google
            </div>
            <div id="loginEmailBtn" class="loginBtn" @click=${this.displayLoginUserForm}>
              <i class="fas fa-unlock-alt"></i>
              Email and password
            </div>
            <div id="createUserBtn" class="loginBtn" @click=${this.displayCreateUserForm}>
              <i class="fas fa-plus-circle"></i> Create account
            </div>
          </div>

          <!-- Email account creation form -->
          <div id="createUserForm" ?hidden=${this._loginBoxStep !== 'createUserForm'}>
            <div class="title">Create account</div>
            <input id="createUserEmail" type="email" placeholder="Email" required />
            <input id="createUserPass1" type="password" placeholder="Password" minlength="6" required />
            <input id="createUserPass2" type="password" placeholder="Confirm your password" minlength="6" required />
            <div id="createUserError"></div>
            <button name="submit" @click=${this.createEmailAccount}>Create account</button>
          </div>

          <!-- Email account login form -->
          <div id="loginUserForm" ?hidden=${this._loginBoxStep !== 'loginUserForm'}>
            <div class="title">Login account</div>
            <input id="loginEmail" type="email" placeholder="Email" required />
            <input id="loginPassword" type="password" placeholder="Password" minlength="6" required />
            <div id="loginUserError"></div>
            <button name="submit" @click=${this.loginUser}>Login</button>
            <div id="passwordForgot" @click=${this.sendPasswordForget}>I forgot! Please send me an email!</div>
          </div>
          <div id="forgotPasswordMsg" ?hidden=${this._loginBoxStep !== 'forgotPasswordEmailSent'}></div>
          <div id="profileEmail">${this.profileEmail ? this.profileEmail : ''}</div>
          <div id="logoutBtn" @click=${this.logout} ?hidden=${!this.isLogged}>Logout</div>
        </div>
      </div>
    `
  }

  static get properties () {
    return {
      debug: Boolean,
      firebaseConfig: Object,
      profilePicture: String,
      profileName: String,
      profileEmail: String,
      isLogged: Boolean,
      user: Object,
      _loginBoxStep: String
    }
  }

  constructor () {
    super()
    this.debug = true
    this.firebaseConfig = null
    this.profilePicture = ''
    this._loginBoxStep = 'loginOptions'
  }

  updated (props) {
    if (props.has('firebaseConfig')) {
      this._initFirebase(this.firebaseConfig)
    }
  }

  async _initFirebase (firebaseConfig) {
    if (!firebaseConfig) {
      firebaseConfig = await this.mixin_fetch('./firebase.json')
    }
    this.mixin_waitFor(
      () => {
        return !!window.firebase
      },
      () => {
        this._firebase = window.firebase

        if (this.debug) {
          this.log('firebaseConfig:', firebaseConfig)
        }

        this._firebase.initializeApp(firebaseConfig)

        /** Listen for the firebase auth event. Mostly used when coming back on
         * the website to detect reloging of the suer.
         */
        this._firebase.auth().onAuthStateChanged((user) => {
          if (user) {
            this.setUser(user)
          } else {
            // User is signed out
          }
        })
      },
      500
    )
  }

  _displayLoginBox () {
    this._loginBoxStep = 'loginOptions'
    const loginBox = this.shadowRoot.getElementById('loginBox')
    if (loginBox.hasAttribute('hidden')) {
      loginBox.removeAttribute('hidden')
    } else {
      loginBox.setAttribute('hidden', true)
    }
    this.fixLoginBoxScreen()
  }

  _hideLoginBox () {
    const loginBox = this.shadowRoot.getElementById('loginBox')
    loginBox.setAttribute('hidden', true)
  }

  fixLoginBoxScreen (setObserver = true) {
    // Width of the body
    const bodyWidth = document.body.clientWidth

    // Size of the wrapper
    const viewportOffset = this.shadowRoot.getElementById('wrapper').getBoundingClientRect()

    // Position of the left side
    const left = viewportOffset.left

    const loginBox = this.shadowRoot.getElementById('loginBox')
    const loginBoxWidth = loginBox.clientWidth
    // Position of the loginBox right side
    const loginBoxPositionRight = left + loginBoxWidth

    // Reset left and right
    loginBox.style.left = 'auto' // Reset
    loginBox.style.right = 'auto' // Reset

    // Adjust position to be in the screen
    if (loginBoxPositionRight > bodyWidth) {
      loginBox.style.right = '5px'
    } else {
      loginBox.style.left = '0px'
    }

    // Add an observer to make sure things are applied
    if (setObserver) {
      let limit = 5
      const interval = setInterval(() => {
        this.fixLoginBoxScreen(false)
        limit--
        if (limit <= 0) {
          clearInterval(interval)
        }
      }, 100)
    }
  }

  loginGoogle () {
    const provider = new this._firebase.auth.GoogleAuthProvider()
    this._firebase.auth().signInWithRedirect(provider)
  }

  displayCreateUserForm () {
    this.shadowRoot.getElementById('createUserError').innerText = ''
    this._loginBoxStep = 'createUserForm'
  }

  createEmailAccount () {
    const errorElement = this.shadowRoot.getElementById('createUserError')
    const emailEl = this.shadowRoot.getElementById('createUserEmail')
    const pass1El = this.shadowRoot.getElementById('createUserPass1')
    const pass2El = this.shadowRoot.getElementById('createUserPass2')
    pass2El.reportValidity()
    pass1El.reportValidity()
    emailEl.reportValidity()

    if (pass1El.value !== pass2El.value) {
      errorElement.innerText = 'Passwords are differents, they should be the same!'
      return
    }

    // All good
    errorElement.innerText = ''

    window.firebase
      .auth()
      .createUserWithEmailAndPassword(emailEl.value, pass1El.value)
      .then((userCredential) => {
        const user = userCredential.user
        console.log('User connected with email account', user)
        this.setUser(user)
        window.firebase
          .auth()
          .currentUser.sendEmailVerification()
          .then(() => {
            console.log('Email verification sent!')
          })
      })
      .catch((error) => {
        const errorCode = error.code
        const errorMessage = error.message
        this.error('Firebase auth', errorCode, errorMessage)
        errorElement.innerText = errorMessage
      })
  }

  displayLoginUserForm () {
    this.shadowRoot.getElementById('loginUserError').innerText = ''
    this._loginBoxStep = 'loginUserForm'
  }

  loginUser () {
    const errorElement = this.shadowRoot.getElementById('loginUserError')
    const emailEl = this.shadowRoot.getElementById('loginEmail')
    const passwordEl = this.shadowRoot.getElementById('loginPassword')
    errorElement.innerText = ''
    passwordEl.reportValidity()
    emailEl.reportValidity()
    window.firebase
      .auth()
      .signInWithEmailAndPassword(emailEl.value, passwordEl.value)
      .then((userCredential) => {
        const user = userCredential.user
        this.setUser(user)
      })
      .catch((error) => {
        const errorCode = error.code
        const errorMessage = error.message
        this.error('Firebase auth', errorCode, errorMessage)
        errorElement.innerText = errorMessage
      })
  }

  logout () {
    this._firebase
      .auth()
      .signOut()
      .then(() => {
        this.log('User logout...')
        this.setUser(null)
        location.reload()
      })
      .catch((error) => {
        this.error('Error while logout', error)
      })
  }

  sendPasswordForget () {
    const errorElement = this.shadowRoot.getElementById('loginUserError')
    const emailEl = this.shadowRoot.getElementById('loginEmail')
    const msgEl = this.shadowRoot.getElementById('forgotPasswordMsg')
    emailEl.reportValidity()
    errorElement.innerText = ''
    window.firebase
      .auth()
      .sendPasswordResetEmail(emailEl.value)
      .then(() => {
        msgEl.innerText = `An email was sent to ${emailEl.value} in order to reset your password.`
        this._loginBoxStep = 'forgotPasswordEmailSent'
        setTimeout(() => {
          this._hideLoginBox()
        }, 5000)
      })
      .catch((error) => {
        const errorCode = error.code
        const errorMessage = error.message
        this.error('Could not send forget password email', errorCode, errorMessage)
        errorElement.innerText = errorMessage
      })
  }

  setUser (user) {
    const evt = new CustomEvent('changed', { detail: { user } })
    this.dispatchEvent(evt)
    if (user) {
      this.isLogged = true
      this.user = user
      this.profileName = user.displayName || user.email
      this.profilePicture = user.photoURL
      this.profileEmail = user.email
      this._hideLoginBox()
    } else {
      this.isLogged = false
      this.user = null
      this.profileName = null
      this.profilePicture = null
    }
  }
}
customElements.define('element-firebase-auth', ElementFirebaseAuth)
export default ElementFirebaseAuth
