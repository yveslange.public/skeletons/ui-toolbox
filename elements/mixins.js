const ElementMixins = (CLASS) => {
  return class extends CLASS {
    constructor () {
      super()
    }

    print (mode = 'log', fgcolor = '#333', bgcolor = '#eee', msg) {
      this.nodeNameConsole = [`%c${this.nodeName}`, `color:${fgcolor};background-color:${bgcolor};`]
      console[mode](...this.nodeNameConsole, ...msg)
    }

    log (...msg) {
      this.print('log', undefined, undefined, msg)
    }

    trace (...msg) {
      this.print('trace', 'darkslategray', '', msg)
    }

    error (...msg) {
      this.print('error', 'darkred', '', msg)
    }

    warn (...msg) {
      this.print('warn', 'darkorange', '', msg)
    }

    mixin_initCDN (urls, context) {
      importScript(urls, context)
    }

    mixin_debounce (callback, timeout = 100, name = 'default') {
      if (!this.__DEBOUNCERS) {
        this.__DEBOUNCERS = {}
      }
      if (this.__DEBOUNCERS[name]) {
        clearTimeout(this.__DEBOUNCERS[name])
      }
      this.__DEBOUNCERS[name] = setTimeout(() => {
        callback()
      }, timeout)
    }

    mixin_threshold (callback, threshold = 500, name = 'default') {
      const now = new Date().getTime()
      if (!this.__THRESHOLDERS) {
        this.__THRESHOLDERS = {}
        this.__THRESHOLDERS[name] = now
      }
      if (now - threshold < this.__THRESHOLDERS[name]) {
        return
      }
      this.__THRESHOLDERS[name] = now
      if (callback) {
        callback()
      }
    }

    mixin_waitFor (condition, callback, timeout = 100) {
      const interval = setInterval(
        () => {
          if (condition()) {
            clearInterval(interval)
            callback()
          }
        },
        timeout < 10 ? 10 : timeout
      )
    }

    async mixin_sleep (time = 0) {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve()
        }, time)
      })
    }

    async mixin_fetch (url, options) {
      return new Promise((resolve, reject) => {
        fetch(url, options)
          .then((response) => {
            if (response.ok) {
              const contentType = response.headers.get('content-type')
              if (contentType && contentType.indexOf('application/json') !== -1) {
                return response.json().then(function (json) {
                  resolve(json)
                })
              } else {
                resolve(response)
              }
            } else {
              reject(new Error('Error while getting the response'))
            }
          })
          .catch((error) => {
            reject(error)
          })
      })
    }

    mixin_dom (query) {
      const result = Array.from(this.shadowRoot.querySelectorAll(query))
      if (result.length === 1) {
        return result[0]
      }
      return result
    }

    listen (dom, event, callback) {
      dom.addEventListener(event, callback.bind(this))
    }
  }
}

// Type can be JS or CSS
const importScript = (urls, context = document.head) => {
  const globalName = 'ELEMENT_IMPORTED_SCRIPT'
  // Create general reference to track already imported libs
  if (!window[globalName]) {
    window[globalName] = {}
  }
  if (!Array.isArray(urls)) {
    urls = [urls]
  }
  urls.forEach((url) => {
    let scriptElement = null
    if (!window[globalName][url]) {
      scriptElement = document.createElement('script')
      scriptElement.setAttribute('crossorigin', 'anonymous')
      scriptElement.src = url
      window[globalName][url] = scriptElement
      context.appendChild(scriptElement)
    }
  })
}

const importCSS = (urls, context = document.head) => {
  const globalName = 'ELEMENT_IMPORTED_CSS'
  // Create general reference to track already imported libs
  if (!window[globalName]) {
    window[globalName] = {}
  }
  if (!Array.isArray(urls)) {
    urls = [urls]
  }
  urls.forEach((url) => {
    let scriptElement = null
    if (!window[globalName][url]) {
      scriptElement = document.createElement('link')
      scriptElement.setAttribute('rel', 'stylesheet')
      scriptElement.href = url
      window[globalName][url] = scriptElement
      context.appendChild(scriptElement)
    }
  })
}

export default ElementMixins
export { ElementMixins, importScript, importCSS }
